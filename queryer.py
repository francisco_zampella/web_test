import os
import random

import requests
import time
from bs4 import BeautifulSoup


def get_random_word():
    page = requests.get('https://randomword.com/')
    soup = BeautifulSoup(page.text, 'html.parser')
    for div in soup.find_all('div'):
        if div.has_attr('id') and 'random_word' == div.get('id'):
            return div.text
    return ''


if __name__ == '__main__':
    REQUEST_URL = 'https://www.bing.com/search?q='
    chrome = 'C:\\Users\\Franciscoz1\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe '
    profiles = ['--profile-directory="Profile 2" ', '--profile-directory="Profile 1" ']

    n_searches = 30
    for profile in profiles:
        os.system(chrome + profile + '--new-window "www.bing.com"')
        time.sleep(1)
        test_words = []
        tries = 0
        while len(test_words) < n_searches:
            tries += 1
            test_word = get_random_word()
            print('{}: {}'.format(tries, test_word))
            if len(test_word) > 0 and test_word not in test_words:
                os.system(chrome + profile + REQUEST_URL + test_word)
                test_words.append(test_word)
                time.sleep(2 + random.randint(0, 3))
